package no.storebrand;

import com.enonic.xp.branch.Branch;
import com.enonic.xp.content.*;
import com.enonic.xp.context.Context;
import com.enonic.xp.context.ContextAccessor;
import com.enonic.xp.context.ContextBuilder;
import com.enonic.xp.data.PropertyTree;
import com.enonic.xp.export.ExportService;
import com.enonic.xp.export.ImportNodesParams;
import com.enonic.xp.export.NodeImportResult;
import com.enonic.xp.index.IndexService;
import com.enonic.xp.node.NodePath;
import com.enonic.xp.schema.content.ContentTypeName;
import com.enonic.xp.security.*;
import com.enonic.xp.security.acl.AccessControlEntry;
import com.enonic.xp.security.acl.AccessControlList;
import com.enonic.xp.security.acl.Permission;
import com.enonic.xp.security.auth.AuthenticationInfo;
import com.enonic.xp.vfs.VirtualFile;
import com.enonic.xp.vfs.VirtualFiles;
import org.osgi.framework.Bundle;
import org.osgi.framework.FrameworkUtil;
import org.osgi.service.component.annotations.Activate;
import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

@Component(immediate = true)
public final class ContentImport {

    private static final AccessControlList PERMISSIONS =
            AccessControlList.of(AccessControlEntry.create().principal(RoleKeys.EVERYONE).allow(Permission.READ).build(),
                    AccessControlEntry.create().principal(RoleKeys.CONTENT_MANAGER_ADMIN).allowAll().build(),
                    AccessControlEntry.create().principal(RoleKeys.CONTENT_MANAGER_APP).allow(Permission.READ).build(),
                    AccessControlEntry.create().principal(RoleKeys.ADMIN).allowAll().build());

    private ContentService contentService;
    private ExportService exportService;
    private SecurityService securityService;
    private IndexService indexService;

    private final Logger LOG = LoggerFactory.getLogger(ContentImport.class);

    private static final PrincipalKey SUPER_USER_KEY = PrincipalKey.ofUser(UserStoreKey.system(), "su");

    private final String[] importContentPaths = {"/delphi"};
    private List<ContentId> createdIds = new ArrayList<>();

    @Activate
    public void initialize()
            throws Exception {
        if (this.indexService.isMaster()) {
            runAs(createContentContext(), () -> {
                importContent(importContentPaths);
                return null;
            });
        }
    }

    private Context createContentContext() {
        return ContextBuilder.from(ContextAccessor.current()).
                authInfo(AuthenticationInfo.create().principals(RoleKeys.CONTENT_MANAGER_ADMIN).user(User.ANONYMOUS).build()).
                branch(ContentConstants.BRANCH_DRAFT).
                repositoryId(ContentConstants.CONTENT_REPO.getId()).
                build();
    }

    private void importContent(String[] paths)
            throws Exception {
        for (String path : paths) {
            LOG.info("import path: " + path);
            importPath(path, "/content");
            LOG.info("imported path: " + path);

        }
        createdIds.forEach(createdId -> {
            final PushContentParams pushContentParams = PushContentParams.create().contentIds(ContentIds.from(createdId)).target(Branch.from("master")).build();
            contentService.push(pushContentParams);
        });
        createdIds.clear();
    }


    private void importPath(String path, String nodePath) {
        final ContentPath importPath = ContentPath.from(path);
        if (hasContent(importPath)) {
            return;
        }

        final Bundle bundle = FrameworkUtil.getBundle(this.getClass());

        final VirtualFile source = VirtualFiles.from(bundle, "/import/content" + path);

        final NodeImportResult nodeImportResult = this.exportService.importNodes(ImportNodesParams.create().
                source(source).
                targetNodePath(NodePath.create(nodePath).build()).
                includeNodeIds(true).
                dryRun(false).
                build());

        logImport(nodeImportResult);

        // set permissions
        final Content importContent = contentService.getByPath(importPath);
        if (importContent != null) {
            final UpdateContentParams setContentPermissions = new UpdateContentParams().
                    contentId(importContent.getId()).
                    editor((content) -> {
                        content.permissions = PERMISSIONS;
                        content.inheritPermissions = false;
                    });
            contentService.update(setContentPermissions);

            contentService.applyPermissions(ApplyContentPermissionsParams.create().
                    contentId(importContent.getId()).
                    overwriteChildPermissions(true).
                    build());

            createdIds.add(importContent.getId());
        }
    }

    private void logImport(final NodeImportResult nodeImportResult) {
        LOG.info("-------------------");
        LOG.info("Imported nodes:");
        for (final NodePath nodePath : nodeImportResult.getAddedNodes()) {
            LOG.info(nodePath.toString());
        }

        LOG.info("-------------------");
        LOG.info("Binaries:");
        nodeImportResult.getExportedBinaries().forEach(LOG::info);

        LOG.info("-------------------");
        LOG.info("Errors:");
        for (final NodeImportResult.ImportError importError : nodeImportResult.getImportErrors()) {
            LOG.info(importError.getMessage(), importError.getException());
        }
    }

    private CreateContentParams.Builder makeFolder() {
        return CreateContentParams.create().
                owner(PrincipalKey.ofAnonymous()).
                contentData(new PropertyTree()).
                type(ContentTypeName.folder()).
                inheritPermissions(true);
    }

    private boolean hasContent(final ContentPath path) {
        try {
            return this.contentService.getByPath(path) != null;
        } catch (final Exception e) {
            return false;
        }
    }

    @Reference
    public void setExportService(final ExportService exportService) {
        this.exportService = exportService;
    }

    @Reference
    public void setContentService(final ContentService contentService) {
        this.contentService = contentService;
    }

    @Reference
    public void setIndexService(final IndexService indexService) {
        this.indexService = indexService;
    }

    private <T> T runAs(final Context context, final Callable<T> runnable) {
        return context.callWith(runnable);
    }
}