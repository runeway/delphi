# delphi

#Content export / import

##This is how to generate new export files from server:
[server]\toolbox>toolbox.bat export -p 8080 -a su:password -s cms-repo:draft:/content/delphi -t delphi
(or POST request to endpoint /api/repo/export with json body according to enonic doc)

##How to import the exported content: 
Replace the content in src/main/resources/import/content/delphi with the content in 
[enonic-xp-home]/data/export/delphi/delphi

Delete (or rename) the repo folder in your Enonic XP home directoy. Build the project and then restart the Enonic server. 
The content in src/main/resources/import/content/* will be added to your new repo folder.